/**
 * @file
 *   Conekta javascript funcitonality.
 */

(function commerce_conekta_gateway_form($, Drupal, drupalSettings) {
  'use strict';

  /**
   * Short description.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.conektaForm = {
    attach: function (context) {
      if (!drupalSettings.commerceConekta.publishableKey) {
        return;
      }

      var attempts = 0;

      var $form = $('.conekta-form', context).closest('form');

      const processForm = function () {
        try {
          // Initialize the Conekta library.
          Conekta.setPublicKey(drupalSettings.commerceConekta.publishableKey);
        }
        catch (e) {
          // If for any reason we fail, disable the submit button and hide the card form.
          $form.find(':input.button--primary').prop('disabled', true);
          $form.find('[data-conekta]').attr('disabled', 'disabled');

          var tryAgain = function () {
            if (attempts < 3) {
              console.log('Trying to initialize Conekta. Attempt: ' + attempts);
              attempts++;
              processForm();
            }
            else {
              console.log('Failed to load Conekta.');
            }
          };

          setTimeout(tryAgain, 500);

          return;
        }

        $form.on('submit', function (e) {
          $form.find(".card-errors").removeClass('messages messages--error').text('');
          // Prevents double clic
          $form.find(":input.button--primary").prop("disabled", true);
          // Pull the full
          var formData = Conekta._helpers.parseForm($form);
          formData.card.name = formData.given_name + ' ' + formData.family_name;
          Conekta.Token.create(formData, function () {
            Drupal.behaviors.conektaForm.conektaSuccessResponseHandler.apply($form, arguments);
          }, function () {
            Drupal.behaviors.conektaForm.conektaErrorResponseHandler.apply($form, arguments);
          });
          return false;
        });
        // If the initialization scceeded, enable the card data fields.
        $form.find(':input.button--primary').prop('disabled', false);
        $form.find('[data-conekta]').removeAttr('disabled');
      };

      $('.conekta-form', context).once('conekta-processed').each(processForm);
    },

    conektaSuccessResponseHandler: function (token) {
      var $form = $(this);
      //Add the token_id in the form
      $form.find('#conektaTokenId').val(token.id);
      $form.get(0).submit(); //Submit
    },

    conektaErrorResponseHandler: function (response) {
      var $form = $(this);
      $form.find(".card-errors")
        .text(response.message_to_purchaser)
        .addClass('messages messages--error');
      $form.find(":input.button--primary").prop("disabled", false);
    }
  };
}(jQuery, Drupal, drupalSettings));