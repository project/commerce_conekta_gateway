<?php

namespace Drupal\commerce_conekta_gateway\PluginForm\Onsite;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 *
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    $element = parent::buildCreditCardForm($element, $form_state);
    $element['#attributes']['class'][] = 'conekta-form';

    $element['number'] = [
      '#type' => 'item',
      '#markup' => Markup::create(
        '<input class="conekta-card-number form-text form-control required" type="text" size="20" data-conekta="card[number]">'
      ),
      '#title_display' => 'before',
      '#title' => $this->t('Card number'),
      '#label_attributes' => [
        'class' => ['control-label js-form-required form-required'],
      ],
    ];

    $element['expiration']['month'] = [
      '#type' => 'item',
      '#markup' => Markup::create(
        '<input type="text" size="2" data-conekta="card[exp_month]" class="conekta-exp-month form-text form-control required" placeholder="'
          . $this->t('MM')
          . '">'
      ),
      '#title_display' => 'before',
      '#title' => $this->t('Month'),
      '#label_attributes' => [
        'class' => ['control-label js-form-required form-required'],
      ],
    ];

    $element['expiration']['year'] = [
      '#type' => 'item',
      '#markup' => Markup::create(
        '<input type="text" size="4" data-conekta="card[exp_year]" class="conekta-exp-year form-text form-control required" placeholder="'
        . $this->t('YYYY')
        . '">'
      ),
      '#title_display' => 'before',
      '#title' => $this->t('Year'),
      '#label_attributes' => [
        'class' => ['control-label js-form-required form-required'],
      ],
    ];

    $element['security_code'] = [
      '#type' => 'item',
      '#markup' => Markup::create(
        '<input type="text" size="4" data-conekta="card[cvc]" class="conekta-exp-cvv form-text form-control required">'
      ),
      '#title_display' => 'before',
      '#title' => $this->t('CVV'),
      '#label_attributes' => [
        'class' => ['control-label js-form-required form-required'],
      ],
    ];

    $element['conektaTokenId'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'conektaTokenId',
      ],
    ];

    $element['errors'] = [
      '#weight' => -10,
      '#type' => 'container',
      '#attributes' => [
        'class' => ['card-errors'],
      ],
    ];

    $element['#attached']['library'][] = 'commerce_conekta_gateway/form';

    $element['#attached']['drupalSettings']['commerceConekta'] = [
      'publishableKey' => $this->plugin->getPublishableKey(),
    ];

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($this->entity);
    $cacheability->setCacheMaxAge(0);
    $cacheability->applyTo($element);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // Conekta validates the form using js.
    // Make sure the tokenized card info is there.
    $value_path = [
      'payment_information',
      'add_payment_method',
      'payment_details',
    ];

    if ($form_state->has($value_path)) {
      $token = $form_state->getValue(['payment_information', 'add_payment_method',
        'payment_details', 'conektaTokenId',
      ]);
      if (!$token) {
        $form_state->setErrorByName('conektaTokenId', "An error ocurred while processing your credit card.");
      }
    }

    if ($form_state->has(['add_payment_method', 'payment_details'])) {
      $token = $form_state->getValue(['payment_method', 'payment_details',
        'conektaTokenId',
      ]);
      if (!$token) {
        $form_state->setErrorByName('conektaTokenId', "An error ocurred when processing your credit card.");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    if ($email = $form_state->getValue(['contact_information', 'email'])) {
      $email_parents = array_merge($element['#parents'], ['email']);
      $form_state->setValue($email_parents, $email);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    if (isset($form['billing_information'])) {
      $form['billing_information']['#after_build'][] = [get_class($this), 'addAddressAttributes'];
    }

    return $form;
  }

  /**
   * Element #after_build callback: adds "data-conekta" to billing name.
   *
   * This allows our JavaScript to pass these values to Conekta as customer
   * information.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The modified form element.
   */
  public static function addAddressAttributes(array $element, FormStateInterface $form_state) {
    $element['address']['widget'][0]['address']['given_name']['#attributes']['data-conekta'] = 'given_name';
    $element['address']['widget'][0]['address']['family_name']['#attributes']['data-conekta'] = 'family_name';
    $element['address']['widget'][0]['address']['address_line1']['#attributes']['data-conekta'] = 'card[address][street1]';
    $element['address']['widget'][0]['address']['address_line2']['#attributes']['data-conekta'] = 'card[address][street2]';
    $element['address']['widget'][0]['address']['locality']['#attributes']['data-conekta'] = 'card[address][city]';
    $element['address']['widget'][0]['address']['postal_code']['#attributes']['data-conekta'] = 'card[address][zip]';
    // Country code is a sub-element and needs another callback.
    $element['address']['widget'][0]['address']['country_code']['#pre_render'][] = [get_called_class(), 'addCountryCodeAttributes'];

    return $element;
  }

  /**
   * Adds the necessary conekta tags.
   */
  public static function addCountryCodeAttributes(array $element) {
    $element['address']['widget'][0]['address']['country_code']['#attributes']['data-conekta'] = 'card[address][country]';

    return $element;
  }

}
