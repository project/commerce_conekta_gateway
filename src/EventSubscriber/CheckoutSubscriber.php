<?php

namespace Drupal\commerce_conekta_gateway\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class CheckoutSubscriber.
 */
class CheckoutSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['commerce_checkout.completion_register'] = ['commerceCheckoutCompletionRegister'];

    return $events;
  }

  /**
   * This method is called when the commerce_checkout.completion_register is dispatched.
   *
   * @param \Drupal\commerce_checkout\Event\CheckoutCompletionRegisterEvent $event
   *   The dispatched event.
   */
  public function commerceCheckoutCompletionRegister(Event $event) {
    /** @var \Drupal\Core\Session\AccountInterface $account */
    $account = $event->getAccount();
    $order = $event->getOrder();
    /** @var Drupal\commerce_payment\Entity\PaymentGateway $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->entity;
    $conekta = $payment_gateway->getPlugin();

    if ($cust_id = \Drupal::request()->getSession()->get('commerce_conekta_gateway_customer_id')) {
      $conekta->saveRemoteGuestCustomerId($account, $cust_id);
      $account->save();
      \Drupal::request()->getSession()->remove('commerce_conekta_gateway_customer_id');
    }
  }

}
