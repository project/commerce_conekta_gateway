<?php

namespace Drupal\commerce_conekta_gateway\Plugin\Commerce\PaymentGateway;

use Conekta\Conekta as ConektaApi;
use Conekta\ProcessingError;
use Conekta\Order;
use Conekta\Handler;
use Conekta\ParameterValidationError;
use Conekta\ProccessingError;
use Conekta\Customer;
use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use InvalidArgumentException;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "conekta_onsite",
 *   label = "Conekta (Onsite)",
 *   display_label = "Conekta",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_conekta_gateway\PluginForm\Onsite\PaymentMethodAddForm",
 *     "edit-payment-method" = "Drupal\commerce_payment\PluginForm\PaymentMethodEditForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "mastercard", "visa",
 *   },
 * )
 */
class Conekta extends OnsitePaymentGatewayBase implements ConektaInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->initializeApi();
  }

  /**
   * Initialize the library on wakeup.
   */
  public function __wakeup() {
    parent::__wakeup();
    $this->initializeApi();
  }

  /**
   * Initialize the payment gateway library.
   */
  private function initializeApi() {
    ConektaApi::setApiKey($this->configuration['private_key']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => '',
      'private_key' => '',
      'mode' => 'test',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Key'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];
    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key'),
      '#default_value' => $this->configuration['private_key'],
      '#required' => TRUE,
    ];
    $form['mode']['#description'] = $this->t('Payment methods created in one mode will not be available on the other. Still, <strong>if the api keys are for production charges will be made!</strong>');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_key'] = $values['api_key'];
      $this->configuration['private_key'] = $values['private_key'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    try {
      $this->assertPaymentState($payment, ['new']);
      $payment_method = $payment->getPaymentMethod();
      $this->assertPaymentMethod($payment_method);
    } catch (InvalidArgumentException $e) {
      \Drupal::logger('commerce_conekta_gateway')->critical($e->getMessage());
      throw new HardDeclineException("There was an error while trying to perform the payment. No charge was made.");
    }


    // Add a built in test for testing decline exceptions.
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address */
    if ($billing_address = $payment_method->getBillingProfile()) {
      $billing_address = $payment_method->getBillingProfile()->get('address')->first();
      if ($billing_address->getPostalCode() == '53140') {
        throw new HardDeclineException('The payment was declined');
      }
    }

    $items = [];
    foreach ($payment->getOrder()->getItems() as $order_item) {
      $item = [
        'name' => $order_item->getTitle(),
        'unit_price' => (int) ($order_item->getUnitPrice()->getNumber() * 100),
        'quantity' => (int) $order_item->getQuantity(),
      ];
      $items[] = $item;
    }

    $shipping_lines = [];
    $shipping_contact = [];
    $shipments = $payment->getOrder()->get('shipments');
    foreach ($shipments as $shipment) {
      /** @var \Drupal\commerce_shipping\Entity\Shipment $shipment */
      $shipment = $shipment->entity;
      $address = $shipment->getShippingProfile()->get('address')->first();
      $shipping_lines[] = [
        'amount' => (int) ($shipment->getAmount()->getNumber() * 100),
        'carrier' => $shipment->getShippingMethod()->getName(),
      ];
      $shipping_contact = [
        'address' => [
          'street1' => $address->address_line1,
          'street2' => $address->address_line2,
          'postal_code' => $address->postal_code,
          'country' => $address->country_code,
        ],
      ];
    };

    $customer_id = $this->getRemoteCustomerId($payment->getPaymentMethod()->getOwner());
    if (!$customer_id) {
      $customer_id = \Drupal::request()->getSession()->get('commerce_conekta_gateway_customer_id');
    }

    $order = [
      'line_items' => $items,
      'currency' => $payment->getAmount()->getCurrencyCode(),
      'customer_info' => [
        'customer_id' => $customer_id,
      ],
      'metadata' => ['order_id' => $payment->getOrderId()],
      'charges' => [
        [
          'payment_method' => [
            'type' => 'card',
            'payment_source_id' => $payment_method->getRemoteId(),
          ],
        ],
      ],
    ];

    // Only add shipping information if the order contains shipments.
    if ($shipping_lines) {
      $order['shipping_lines'] = $shipping_lines;
      $order['shipping_contact'] = $shipping_contact;
    }

    try {
      $charged_order = Order::create($order);
    }
    catch (ProcessingError $e) {
      \Drupal::logger('commerce_conekta_gateway')->error($e->getMessage());
      throw new PaymentGatewayException("An error ocurred when processing the credit card with Conekta.");
    }
    catch (ParameterValidationError $e) {
      \Drupal::logger('commerce_conekta_gateway')->error($e->getMessage());
      throw new InvalidRequestException("Invalid request when processing the credit card with Conekta.");
    }
    catch (Handler $e) {
      \Drupal::logger('commerce_conekta_gateway')->error($e->getMessage());
      throw new InvalidRequestException("Invalid request when processing the credit card with Conekta.");
    }

    // Perform the create payment request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Remember to take into account $capture when performing the request.
    $amount = $payment->getAmount();
    $payment_method_token = $payment_method->getRemoteId();
    $payment->getOrder()->setData('conekta_order_id', $charged_order->id);
    $payment->getOrder()->save();
    $next_state = $capture ? 'completed' : 'authorization';

    $payment->setState($next_state);
    $payment->setRemoteId($charged_order->charges[0]->id);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    // Perform the capture request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    // Perform the void request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'conektaTokenId',
    ];

    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new InvalidRequestException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    // Add a built in test for testing decline exceptions.
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address */
    if ($billing_address = $payment_method->getBillingProfile()) {
      $billing_address = $payment_method->getBillingProfile()->get('address')->first();
      if ($billing_address->getPostalCode() == '53141') {
        throw new HardDeclineException('The payment method was declined');
      }
    }

    $owner = $payment_method->getOwner();

    if (!$owner) {
      \Drupal::logger('commerce_conekta_gateway')->critical('No user found when trying to add a payment method');
      throw new ParameterValidationError("No user found to make the checkout.");
    }

    // Payment method (named source in conekta)
    $source = NULL;

    try {
      $customer_id = $this->getRemoteCustomerId($owner);

      if ($owner->isAuthenticated() && !$customer_id) {
        // The user is authenticated but it doesn't have a conekta Customer.
        // Create a customer and add him a payment method.
        $customer = $this->createConektaCustomerFromUser($owner, $payment_details);
        $source = $customer->payment_sources[0];
      }
      elseif ($owner->isAuthenticated() && $customer_id) {
        // The user already has a customer in Conekta, lets add a
        // payment method.
        $customer = Customer::find($customer_id);
        $source = $customer->createPaymentSource([
          'token_id' => $payment_details['conektaTokenId'],
          'type'     => 'card',
        ]);
      }
      else {
        // The user is not authenticated.
        // Create a Conekta customer and if he decides to create an account
        // later on, make the relationship.
        $customer = $this->createConektaUserFromBillingAddress($billing_address, $payment_details);
        // Save in the session the conekta customer id
        // so we can use it later.
        \Drupal::request()->getSession()->set('commerce_conekta_gateway_customer_id', $customer->id);
        $source = $customer->payment_sources[0];
      }
    }
    catch (ProcessingError $error) {
      \Drupal::logger('comerce_conekta')->critical($error->getMessage());
      throw new HardDeclineException();
    }
    catch (ParameterValidationError $error) {
      \Drupal::logger('comerce_conekta')->critical($error->getMessage());
      throw new InvalidRequestException("Conekta customer creation failed");
    }
    catch (Handler $error) {
      \Drupal::logger('comerce_conekta')->critical($error->getMessage());
      throw new InvalidRequestException("There was an error communicating with Conekta");
    }
    $this->setRemoteCustomerId($owner, (string) $customer->id);
    $owner->save();

    // Non-reusable payment methods usually have an expiration timestamp.
    $payment_method->card_type = $this->translateBrand($source->brand);
    // Only the last 4 numbers are safe to store.
    $payment_method->card_number = $source->last4;
    $payment_method->card_exp_month = $source->exp_month;
    $payment_method->card_exp_year = $source->exp_year;
    $expires = CreditCard::calculateExpirationTimestamp($source->exp_month, $source->exp_year);
    $payment_method->setRemoteId($source->id);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * Translates credit card types to the expected type by commerce.
   *
   * @param string $brand
   *   The credit card type as returned by Conekta.
   *
   * @return
   *   The credit card type as expected by Commerce.
   **/
  public function translateBrand($brand) {
    $type = '';
    switch ($brand) {
      case 'AMERICAN_EXPRESS':
        $type = 'amex';
        break;

      default:
        $type = strtolower($brand);
        break;
    }
    return $type;
  }

  /**
   * Creates a remote customer from an authenticated user.
   *
   * @param Drupal\user\UserInterface $owner
   *   The authenticated Drupal user.
   * @param array $payment_details
   *   The payment method form data.
   *
   * @return \Conekta\Customer
   *   The newly created user, if successfull.
   */
  protected function createConektaCustomerFromUser(UserInterface $owner, array $payment_details) {
    $data = [
      "name" => $owner->getDisplayName(),
      "email" => $owner->getEmail(),
      "metadata" => ['uid' => $owner->id()],
      "payment_sources" => [
          [
            "type" => "card",
            "token_id" => $payment_details['conektaTokenId'],
          ],
      ],
    ];
    if ($owner->hasField('field_phone') && $owner->get('field_phone')->value) {
      $data['phone'] = $owner->get('field_phone')->value;
    }

    $customer = Customer::create($data);
    return $customer;
  }

  /**
   * Creates a Conekta customer from a billing address.
   *
   * This is used on Guest Checkout, when the user is not yet authenticated but
   * we have enough data to register a payment method (or "source" in Conekta).
   *
   * @param Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address
   *   The posted billing address.
   * @param array $payment_details
   *   The add payment method form values with the email.
   *
   * @return \Conekta\Customer
   *   The newly created customer.
   */
  public function createConektaUserFromBillingAddress(AddressItem $billing_address, array $payment_details) {
    $customer = Customer::create([
      "name" => $billing_address->getGivenName() . ' ' . $billing_address->getFamilyName(),
      "email" => $payment_details['email'],
      "metadata" => ['uid' => 'guest'],
      "payment_sources" => [
        [
          "type" => "card",
          "token_id" => $payment_details['conektaTokenId'],
        ],
      ],
    ]);

    return $customer;
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $cust_id = $this->getRemoteCustomerId($payment_method->getOwner());
    try {
      /** @var \Conekta\Customer $customer */
      $customer = Customer::find($cust_id);
      if ($customer) {
        $customer->deletePaymentSourceById($payment_method->getRemoteId());
      }
    }
    catch (\Throwable $e) {
      \Drupal::logger('commerce_conekta_gateway')->error(sprintf("Error deleting payment method. Source: %s, Customer: %s, Message: %s",
        $payment_method->getRemoteId(),
        $cust_id,
        $e->getMessage()));
    }

    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    // The default payment method edit form only supports updating billing info.
    $billing_profile = $payment_method->getBillingProfile();

    // Perform the update request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
  }

  /**
   * Retrieves the publishable key from the configuration.
   *
   * @return string
   *   The configured public API key.
   */
  public function getPublishableKey() {
    return $this->configuration['api_key'];
  }

  /**
   * Saves the pending customer id for guest checkouts.
   *
   * @see Drupal\commerce_conekta_gateway\EventSubscriber\CheckoutSubscriber
   */
  public function saveRemoteGuestCustomerId($account, $remote_id) {
    $this->setRemoteCustomerId($account, $remote_id);
  }

}
